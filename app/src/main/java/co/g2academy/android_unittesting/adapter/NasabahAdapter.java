package co.g2academy.android_unittesting.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import co.g2academy.android_unittesting.R;
import co.g2academy.android_unittesting.ViewActivity;
import co.g2academy.android_unittesting.model.Nasabah;

public class NasabahAdapter extends RecyclerView.Adapter<NasabahAdapter.NasabahViewHolder> {

    Context context;
    ArrayList<Nasabah> nasabahs;

    public NasabahAdapter(Context context, ArrayList<Nasabah> nasabahs) {
        this.context = context;
        this.nasabahs = nasabahs;
    }

    @NonNull
    @Override
    public NasabahViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_nasabah, parent, false);
        return new  NasabahViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NasabahViewHolder holder, int position) {
        holder.namaTv.setText(nasabahs.get(position).getNama());
        holder.alamatTv.setText(nasabahs.get(position).getAlamat());
        holder.emailTv.setText(nasabahs.get(position).getEmail());

        holder.nasabahLl.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(),nasabahs.get(position).getNama(), Toast.LENGTH_SHORT).show();
                Intent intent =
                        new Intent( context, co.g2academy.android_unittesting.ViewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("mode", "edit");
                bundle.putString("id", nasabahs.get(position).getId().toString());
                bundle.putString("nama", nasabahs.get(position).getNama());
                bundle.putString("alamat", nasabahs.get(position).getAlamat());
                bundle.putString("email", nasabahs.get(position).getEmail());
                intent.putExtras(bundle);
                context.startActivity(intent);
            };
        });
//        Picasso.get().load(nasabahs.get(position).getUrlToImage()).into(holder.nasabahIv);
    }

    @Override
    public int getItemCount() {
        return nasabahs.size();
    }

    public class NasabahViewHolder extends RecyclerView.ViewHolder{

        TextView namaTv;
        TextView alamatTv;
        TextView emailTv;
        ImageView nasabahIv;
        LinearLayout nasabahLl;

        public NasabahViewHolder(@NonNull View itemView) {
            super(itemView);

            namaTv = itemView.findViewById(R.id.namaTextView);
            alamatTv = itemView.findViewById(R.id.alamatTextView);
            emailTv = itemView.findViewById(R.id.emailTextView);
            nasabahIv = itemView.findViewById(R.id.nasabahImageView);
            nasabahLl = itemView.findViewById(R.id.nasabahLl);

        }
    }



}
