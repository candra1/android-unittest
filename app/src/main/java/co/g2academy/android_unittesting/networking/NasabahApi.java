package co.g2academy.android_unittesting.networking;

import co.g2academy.android_unittesting.model.Nasabah;
import co.g2academy.android_unittesting.model.NasabahResponse;
import co.g2academy.android_unittesting.model.NasabahsResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface NasabahApi {
    @GET("nasabah")
    Call<NasabahsResponse> getNasabahsList(@Query("page") String page,
                                       @Query("limit") String limit);

    @POST("nasabah")
    Call<NasabahResponse> postNasabah(@Body Nasabah body);

    @GET("nasabah/{id}")
    Call<NasabahResponse> getNasabah(@Path("id") String id);

    @PUT("nasabah/{id}")
    Call<NasabahResponse> putNasabah(@Path("id") String id, @Body Nasabah body);

    @DELETE("nasabah/{id}")
    Call<NasabahResponse> deleteNasabah(@Path("id") String id);

}
