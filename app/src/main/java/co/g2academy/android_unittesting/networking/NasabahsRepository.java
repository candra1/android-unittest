package co.g2academy.android_unittesting.networking;

import androidx.lifecycle.MutableLiveData;

import co.g2academy.android_unittesting.model.Nasabah;
import co.g2academy.android_unittesting.model.NasabahResponse;
import co.g2academy.android_unittesting.model.NasabahsResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NasabahsRepository {
    private static NasabahsRepository nasabahsRepository;

    public static NasabahsRepository getInstance(){
        if (nasabahsRepository == null){
            nasabahsRepository = new NasabahsRepository();
        }
        return nasabahsRepository;
    }

    private NasabahApi nasabahApi;

    public NasabahsRepository(){
        nasabahApi = RetrofitService.cteateService(NasabahApi.class);
    }

    public MutableLiveData<NasabahsResponse> getNasabahs(String page, String limit){
        MutableLiveData<NasabahsResponse> nasabahsData = new MutableLiveData<>();
        nasabahApi.getNasabahsList(page, limit).enqueue(new Callback<NasabahsResponse>() {
            @Override
            public void onResponse(Call<NasabahsResponse> call,
                                   Response<NasabahsResponse> response) {
                if (response.isSuccessful()){
                    nasabahsData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<NasabahsResponse> call, Throwable t) {
                nasabahsData.setValue(null);
            }
        });
        return nasabahsData;
    }

    public MutableLiveData<NasabahResponse> getNasabah(String id){
        MutableLiveData<NasabahResponse> nasabahData = new MutableLiveData<>();
        nasabahApi.getNasabah(id).enqueue(new Callback<NasabahResponse>() {
            @Override
            public void onResponse(Call<NasabahResponse> call,
                                   Response<NasabahResponse> response) {
                if (response.isSuccessful()){
                    nasabahData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<NasabahResponse> call, Throwable t) {
                nasabahData.setValue(null);
            }
        });
        return nasabahData;
    }
    public MutableLiveData<NasabahResponse> updateNasabah(String id, Nasabah nasabahPayload){
        MutableLiveData<NasabahResponse> nasabahData = new MutableLiveData<>();
        nasabahApi.putNasabah(id,nasabahPayload).enqueue(new Callback<NasabahResponse>() {
            @Override
            public void onResponse(Call<NasabahResponse> call,
                                   Response<NasabahResponse> response) {
                if (response.isSuccessful()){
                    nasabahData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<NasabahResponse> call, Throwable t) {
                nasabahData.setValue(null);
            }
        });
        return nasabahData;
    }

    public MutableLiveData<NasabahResponse> postNasabah(Nasabah nasabahPayload){
        MutableLiveData<NasabahResponse> nasabahData = new MutableLiveData<>();
        nasabahApi.postNasabah(nasabahPayload).enqueue(new Callback<NasabahResponse>() {
            @Override
            public void onResponse(Call<NasabahResponse> call,
                                   Response<NasabahResponse> response) {
                if (response.isSuccessful()){
                    nasabahData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<NasabahResponse> call, Throwable t) {
                nasabahData.setValue(null);
            }
        });
        return nasabahData;
    }

    public MutableLiveData<NasabahResponse> deleteNasabah(String id){
        MutableLiveData<NasabahResponse> nasabahData = new MutableLiveData<>();
        nasabahApi.deleteNasabah(id).enqueue(new Callback<NasabahResponse>() {
            @Override
            public void onResponse(Call<NasabahResponse> call,
                                   Response<NasabahResponse> response) {
                if (response.isSuccessful()){
                    nasabahData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<NasabahResponse> call, Throwable t) {
                nasabahData.setValue(null);
            }
        });
        return nasabahData;
    }

}
